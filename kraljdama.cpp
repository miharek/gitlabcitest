#include "headers/algorithm.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include "./source/macro.cpp"
#include "headers/Board.h"


std::vector<std::vector<int>> getInput(std::string fileName);
bool safe_dama(int dama_x, int dama_y, int kralj_x, int kralj_y);
bool safe_kralj(int dama_x, int dama_y, int kralj_x, int kralj_y);

int h = 0;
int w = 0;

bool safe_dama(int dama_x, int dama_y, int kralj_x, int kralj_y) {
    bool tmp = std::max(std::abs(dama_x-kralj_x), std::abs(dama_y-kralj_y)) > 1;
    // std::cout << tmp << std::endl;
    return tmp;
}

MEMOIZATION(bool, solve, (int dama_x, int dama_y, int kralj_x, int kralj_y, int t, int m)) {
    if (m < 0) return false;
    // KRALJ
    if (t == 1) {
        for(int dx = -1; dx <= 1; dx++) {
            for(int dy = -1; dy <= 1; dy++) {
                if (dx == 0 && dy == 0) continue;
                int kralj_X = kralj_x + dx;
                int kralj_Y = kralj_y + dy;
                if (safe_kralj(dama_x, dama_y, kralj_X, kralj_Y)) {
                    if(!solve(dama_x, dama_y, kralj_X, kralj_Y, 1-t, m-1)){
                        return false;
                    }
                }
            }
        }
        return true;
    } else { // DAMA
        if (m > 0) {
            Board b(h,w,Position(kralj_x,kralj_y),Position(dama_x,dama_y));

            std::vector<Position> moves;
            for(int i = 0; i < b.getHeight(); i++){
                if(i != b.getQueen().row){
                    if(i != b.getKing().row && b.getQueen().col != b.getKing().col) continue;
                    moves.push_back(Position(i,b.getQueen().col));
                }
            }
            for(int i = 0; i < b.getWidth(); i++){
                if(i != b.getQueen().col){
                    if(i != b.getKing().col && b.getQueen().row != b.getKing().row) continue;
                    moves.push_back(Position(b.getQueen().row,i));
                }
            }
            for(int i = b.getQueen().row -1, j = b.getQueen().col -1; i >=0 && j >= 0; i--, j--){
                if(i != b.getKing().row && j != b.getKing().col) continue;
                 moves.push_back(Position(i,j));
            }
            for(int i = b.getQueen().row  + 1, j = b.getQueen().col -1; i < b.getHeight() && j >= 0; i++, j--){
                if(i != b.getKing().row && j != b.getKing().col) continue;
                 moves.push_back(Position(i,j));
            }
            for(int i = b.getQueen().row -1, j = b.getQueen().col +1; i >=0 && j < b.getWidth(); i--, j++){
                if(i != b.getKing().row && j != b.getKing().col) continue;
                 moves.push_back(Position(i,j));
            }
            for(int i = b.getQueen().row  + 1, j = b.getQueen().col + 1; i < b.getHeight() && j < b.getWidth(); i++, j++){
                if(i != b.getKing().row && j != b.getKing().col) continue;
                 moves.push_back(Position(i,j));
            }
            int counter = 0;
            for(int x = 1; x < w +1; x++) {
                for(int y = 1; y < h +1; y++) {
                    if (x == dama_x && y == dama_y) continue;
                    if (x == dama_x || y == dama_y || x+y == dama_x + dama_y || x-y == dama_x - dama_y) {
                        counter++;
                        //for(auto p : moves){
                        int dama_X = x;
                        int dama_Y = y;
                        if (safe_dama(dama_X, dama_Y, kralj_x, kralj_y)) {
                        //if (b.queenAttacked()) {
                            if(solve(dama_X, dama_Y, kralj_x, kralj_y, 1-t, m-1)){
                                return true;
                            }
                        }
                   // }
                        
                    }
                } 
            }
            
        }
        return false;
    }
}

int main(int argc, char* argv[]) {
    //  Check correct usage
	    //  CI test 8
    if (argc != 2) {
        std::cout << "Usage: ./seminarska <filename>\n";
        return 1;
    }

    std::vector<std::vector<int>> data = getInput(argv[1]);

    // Return if file couldn't be opened
    if (data.size() == 0) {
        return 0;
    }
    
    int const HEIGHT = data[0][0];
    int const WIDTH = data[0][1];
    h = HEIGHT;
    w = WIDTH;
    int const P = data[1][0];
    // std::cout << "Height: " << HEIGHT << ", Width: " <<  WIDTH << ", Tests: " << P << std::endl;
    Algorithm *a = new Algorithm(HEIGHT, WIDTH);

    for(int p = 0; p < P; p++) {
        int ky = data[p+2][0];
        int kx = data[p+2][1];
        int dy = data[p+2][2];
        int dx = data[p+2][3];
        bool solved = false;
        int m = 25;
        for(int m = 1; m < 25; m+=2) {
            //std::cout << m << std::endl;
            if(solve(dx, dy, kx, ky, 0, m)) {
                solved = true;
                std::cout << m << std::endl;
                break;
            }
        }
        if(!solved) {
            std::cout << "KRUCIFIKS" << std::endl;
        }
    }

}

bool safe_kralj(int dama_x, int dama_y, int kralj_x, int kralj_y) {
    if (kralj_x <= 0 || kralj_y <= 0) return false;
    if (kralj_x > w || kralj_y > h) return false;
    if (dama_x == kralj_x || dama_y == kralj_y) return false;
    if (dama_x + dama_y == kralj_x + kralj_y || dama_x - dama_y == kralj_x - kralj_y) return false;
    // std::cout << "not" << std::endl;
    return true;
}

std::vector<std::vector<int>> getInput(std::string fileName) {
  std::vector<std::vector<int>> data;
  std::ifstream file(fileName);
  if (file.is_open())
  {    
    std::string line;
    while (std::getline(file, line))
    {
      std::vector<int> lineData;
      std::stringstream lineStream(line);

      int value;

      while (lineStream >> value)
      {
        lineData.push_back(value);
      }

      data.push_back(lineData);
    }
    file.close();
  }
  else
  {
    std::cout << "Unable to open file!\n";        
  }
  return data;
}

