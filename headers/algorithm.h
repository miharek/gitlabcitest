//#pragma once


class Algorithm {
    private:
        int h; 
        int w;
    public:
        Algorithm(int h, int w);
        bool safe_dama(int dama_x, int dama_y, int kralj_x, int kralj_y);
        bool safe_kralj(int dama_x, int dama_y, int kralj_x, int kralj_y);
        bool solve(int dama_x, int dama_y, int kralj_x, int kralj_y, int t, int m);
};