#pragma once

#define KING 1
#define QUEEN -1

struct Position{
    int row,col;

    Position(int r, int c): row(r),col(c){}
};

class Board
{
private:
    int height, width;
    Position king, queen;
public:
    Board(int h, int w, Position k,Position q);

    bool queenAttacked();
    bool kingAttacked();

    Position getKing();
    Position getQueen();

    void setKing(Position p);
    void setQueen(Position p);

    int getHeight();
    int getWidth();

    void printBoard();
};

