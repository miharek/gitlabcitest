#include "../headers/algorithm.h"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <map>
#include "macro.cpp"

Algorithm::Algorithm(int h, int w) {
    h = h;
    w = w;
}

bool Algorithm::safe_dama(int dama_x, int dama_y, int kralj_x, int kralj_y) {
    bool tmp = std::max(std::abs(dama_x-kralj_x), std::abs(dama_y-kralj_y)) > 1;
    // std::cout << tmp << std::endl;
    return tmp;
}

bool Algorithm::safe_kralj(int dama_x, int dama_y, int kralj_x, int kralj_y) {
    if (kralj_x <= 0 || kralj_y <= 0) return false;
    if (kralj_x > w || kralj_y > h) return false;
    if (dama_x == kralj_x || dama_y == kralj_y) return false;
    if (dama_x + dama_y == kralj_x + kralj_y || dama_x - dama_y == kralj_x - kralj_y) return false;
    // std::cout << "not" << std::endl;
    return true;
}



bool Algorithm::solve(int dama_x, int dama_y, int kralj_x, int kralj_y, int t, int m=0) {
    return solve(dama_x, dama_y, kralj_x, kralj_y, t, m);
}
