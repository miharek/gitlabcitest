#include  <map>
#include  <tuple>

#define MEMOIZATION(ret,name,params) \
  ret name##nonmemo params; \
  auto name = memo_wrapper(name##nonmemo);\
  ret name##nonmemo params

template<class...Arg> struct Memo;


template<class R, class...Arg>
struct Memo<R(*)(Arg...)>
{    
    using funcpoint_t =  R(*)(Arg...a);
    using tuple_t= std::tuple<Arg...>;
    using map_t = std::map<tuple_t,R>;
    
    Memo(funcpoint_t fp):mFp(fp){}
    
    const R&  operator()(Arg...arg)
    {
       tuple_t tup(arg...);
       auto it = mMap.find(tup);
       if(it != mMap.end())
          return it->second;
       else
          return mMap.insert(std::make_pair(tup,mFp(arg...))).first->second;
    }
    
    private:
    funcpoint_t mFp;
    map_t mMap;
};

template <class R, class...Arg>
Memo< R(*)(Arg...)> 
memo_wrapper( R(*f)(Arg...)  ) 
{
    return Memo< R(*)(Arg...)>(f);
}
