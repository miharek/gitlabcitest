#include "../headers/Board.h"

#include <iostream>

Board::Board(int h, int w, Position k, Position q): height(h), width(w), king(k), queen(q){}

void Board::setKing(Position p){
    king = p;
}
void Board::setQueen(Position p){
    queen = p;
}

Position Board::getKing(){
    return king;
}

Position Board::getQueen(){
    return queen;
}

int Board::getHeight(){
    return height;
}

int Board::getWidth(){
    return width;
}

bool Board::queenAttacked(){
    return std::abs(queen.row - king.row) > 1 || std::abs(queen.col -king.col) > 1;
}

bool Board::kingAttacked(){
    // if (kralj_x > w || kralj_y > h) return false;
   // if(dama_x == kralj_x || dama_y == kralj_y) return false;
    // if (dama_x + dama_y == kralj_x + kralj_y || dama_x - dama_y == kralj_x - kralj_y) return false;
    // // std::cout << "not" << std::endl;
    // return true;
}

void Board::printBoard() {
  for (int i = 0; i < height; i++) {
    for (int j = 0; j < width; j++) {
        if(king.row == i && king.col == j){
            std::cout << " K ";
        }
        else if(queen.row == i && queen.col == j){
            std::cout << " D ";
        } 
        else {
            std::cout << " 0 ";
        }
    }
    std::cout << "\n";
  }
}
